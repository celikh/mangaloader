from setuptools import setup

setup(
    name="Manga-Downloader",
    version="1.0",
    py_modules=["main"],
    install_requires=["beautifulsoup4==4.9.3", "click==7.1.2", "requests==2.25.1", "urllib3==1.26.4",
                      "python-slugify==4.0.1"],
    entry_points='''
    [console_scripts]
    mangaloader=main:main
    ''')
