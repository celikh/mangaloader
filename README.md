# mangaloader

Downloads Mangas from: https://www.webtoon.xyz/


## setup:
- clone repo
- run: pip install --editable .

## usage:
- mangaloader download-chapter "\<url>": downloads chapter from given url (ex: https://www.webtoon.xyz/read/solo-leveling/chapter-6/)
- mangaloader download-manga "\<url>": downloads complete manga by extracting chapter (from select picker) urls from given url (ex: https://www.webtoon.xyz/read/solo-leveling/chapter-6/)
