from bs4 import BeautifulSoup
import re
import requests
import click
import os
from slugify import slugify


@click.group()
def main():
    pass


@main.command()
@click.argument('url', type=str, required=True)
@click.pass_context
def download_manga(ctx, url):
    links = []
    url = url
    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')
    # Get all chapter links
    get_details = soup.find_all('option')  # gets list of all <option> tag
    for element in get_details:
        links.append(element["data-redirect"])
    for link in set(links): 
        try:
            #print(url)
            ctx.invoke(download_chapter, url=link)
        except:
            pass


@main.command()
@click.argument('url', type=str, required=True)
def download_chapter(url):
    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')
    # Get chapter name
    chapter_dir_name = soup.find("h1", {"id": "chapter-heading"}).text
    # normalize chapter name; so no error raises when creating a sub-folder in current dir
    chapter_dir_name = slugify(chapter_dir_name, lowercase=False, separator="_")
    # Get current folder path
    current_directory = os.getcwd()
    # Final directory path
    final_directory = os.path.join(current_directory, chapter_dir_name)
    # Check if directory already exists
    if not os.path.exists(final_directory):
        os.makedirs(final_directory)
    for e in soup.find_all("img", {"class": "wp-manga-chapter-img"})[:-2]:
        url = e["data-src"]
        url = re.sub(r"[\n\t\s]*", "", url)
        # Download and save files
        download_jpg_to_dir(final_directory, e["id"], url)


# downloads and saves jpg-files
def download_jpg_to_dir(path, jpg_output_name, url):
    path = os.path.abspath(path)
    #print(path)
    path = os.path.join(path, jpg_output_name + ".jpg")
    print(path)
    with open(path, 'wb') as outfile:
        r = requests.get(url)
        outfile.write(r.content)


main.add_command(download_manga)
main.add_command(download_chapter)

# Press the green button in the gutter to run the script.
# if __name__ == '__main__':
#   print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
